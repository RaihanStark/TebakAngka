
import java.util.*

fun main(args: Array<String>) {

    // Function Math Random
    val random = Random()

    fun rand(from: Int, to: Int): Int {
        return random.nextInt(to - from) + from
    }

    // ulang?
    var ulang = "Y"

    fun permainanTebakan() {
        // Menu
        println("********* Tebak Angka **********")
        println("*                              *")
        println("* Tebak angka melawan komputer *")
        println("* Tuliskan angka 1-10          *")
        println("*                              *")
        println("********* Tebak Angka **********")
        // Input dari user
        print("Masukan Tebakan Angka 1-10: ")
        var tebakPlayer: Int = readLine()!!.toInt()
        // input dari komputer
        var tebakKomputer: Int = rand(1, 10)
        // proses
        if (tebakPlayer == tebakKomputer) {
            println("Selamat Anda Benar!!!\n Jawaban Kamu: $tebakPlayer\n Tebakan Komputer: $tebakKomputer")
            print("Ulangi? Y/N")
            ulang = readLine()!!.toString()

        } else {
            println("Maaf Anfa Salah!!\n Jawaban Kamu: $tebakPlayer\n Tebakan Komputer: $tebakKomputer")
            print("Ulangi? Y/N")
            ulang = readLine()!!.toString()
        }
        // output

    }

    while ((ulang == "Y") or (ulang == "y")) {
        permainanTebakan()
    }
}